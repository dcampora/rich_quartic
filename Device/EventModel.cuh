#include <cassert>
#include <stdio.h>

struct RichPhotons {
protected:
  const float* m_base_pointer;
  const int m_total_number_of_photons;

public:
  __host__ __device__ inline
  RichPhotons(const float* base_pointer, const int total_number_of_photons) :
    m_base_pointer(base_pointer),
    m_total_number_of_photons(total_number_of_photons)
  {}

  __host__ __device__ inline RichPhotons(const RichPhotons& photons) :
    m_base_pointer(photons.m_base_pointer), m_total_number_of_photons(photons.m_total_number_of_photons)
  {}

  // Accessors for all types
  __host__ __device__ inline float emission_point_x(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[index];
  }

  __host__ __device__ inline float emission_point_y(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[m_total_number_of_photons + index];
  }

  __host__ __device__ inline float emission_point_z(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[2 * m_total_number_of_photons + index];
  }

  __host__ __device__ inline float center_of_curve_x(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[3 * m_total_number_of_photons + index];
  }

  __host__ __device__ inline float center_of_curve_y(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[4 * m_total_number_of_photons + index];
  }

  __host__ __device__ inline float center_of_curve_z(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[5 * m_total_number_of_photons + index];
  }

  __host__ __device__ inline float virtual_det_point_x(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[6 * m_total_number_of_photons + index];
  }

  __host__ __device__ inline float virtual_det_point_y(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[7 * m_total_number_of_photons + index];
  }

  __host__ __device__ inline float virtual_det_point_z(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[8 * m_total_number_of_photons + index];
  }

  __host__ __device__ inline float radius(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return m_base_pointer[9 * m_total_number_of_photons + index];
  }

  // float3 accessors
  __host__ __device__ inline float3 emission_point(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return {emission_point_x(index), emission_point_y(index), emission_point_z(index)};
  }

  __host__ __device__ inline float3 center_of_curve(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return {center_of_curve_x(index), center_of_curve_y(index), center_of_curve_z(index)};
  }

  __host__ __device__ inline float3 virtual_det_point(const int index) const
  {
    assert(index < m_total_number_of_photons);
    return {virtual_det_point_x(index), virtual_det_point_y(index), virtual_det_point_z(index)};
  }
};
