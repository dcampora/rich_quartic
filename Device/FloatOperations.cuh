#pragma once

// float2
__host__ __device__ inline float dot(const float2& l, const float2& r) { return l.x * r.x + l.y * r.y; }

__host__ __device__ inline float2 operator+(const float2& l, const float2& r) { return {l.x + r.x, l.y + r.y}; }

__host__ __device__ inline float2 operator-(const float2& l, const float2& r) { return {l.x - r.x, l.y - r.y}; }

__host__ __device__ inline float2 operator*(const float2& l, const float r) { return {l.x * r, l.y * r}; }

__host__ __device__ inline float2 operator/(const float2& l, const float r) { return {l.x / r, l.y / r}; }

__host__ __device__ inline float2& operator+=(float2& l, const float2& r)
{
  l.x += r.x;
  l.y += r.y;
  return l;
}

__host__ __device__ inline float2& operator-=(float2& l, const float2& r)
{
  l.x -= r.x;
  l.y -= r.y;
  return l;
}

// float3

// return {this->Y() * v.z() - v.y() * this->Z(),  //
//               this->Z() * v.x() - v.z() * this->X(),  //
//               this->X() * v.y() - v.x() * this->Y()}; //

__host__ __device__ inline float3 cross(const float3& l, const float3& r) {
  return {l.y * r.z - r.y * l.z,
          l.z * r.x - r.z * l.x,
          l.x * r.y - r.x * l.y};
}

__host__ __device__ inline float dot(const float3& l, const float3& r) { return l.x * r.x + l.y * r.y + l.z * r.z; }

__host__ __device__ inline float3 operator+(const float3& l, const float3& r)
{
  return {l.x + r.x, l.y + r.y, l.z + r.z};
}

__host__ __device__ inline float3 operator-(const float3& l, const float3& r)
{
  return {l.x - r.x, l.y - r.y, l.z - r.z};
}

__host__ __device__ inline float3 operator*(const float3& l, const float r) { return {l.x * r, l.y * r, l.z * r}; }

__host__ __device__ inline float3 operator/(const float3& l, const float r) { return {l.x / r, l.y / r, l.z / r}; }

__host__ __device__ inline float3& operator+=(float3& l, const float3& r)
{
  l.x += r.x;
  l.y += r.y;
  l.z += r.z;
  return l;
}

__host__ __device__ inline float3& operator-=(float3& l, const float3& r)
{
  l.x -= r.x;
  l.y -= r.y;
  l.z -= r.z;
  return l;
}

__host__ __device__ inline float3 operator*(const float3& l, const float3& r) {
  return {l.x * r.x, l.y * r.y, l.z * r.z};
}