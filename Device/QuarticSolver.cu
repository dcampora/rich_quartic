#include "EventModel.cuh"
#include "FloatOperations.cuh"
#include "QuarticSolver.cuh"
#include <cstdio>
#include <limits>
#include <thrust/complex.h>

// Focus on single precision
using Vector = float3;
using float_t = float;
/// Constants
static constexpr int ORDER = 4;
static constexpr int DIFFGRADE = ORDER + 1;
static constexpr float gain = 1.04f;
constexpr static int BISECTITS = 3;
constexpr static int NEWTONITS = 2;

struct Array5 {
  float_t values[DIFFGRADE];

  __host__ __device__ inline float_t operator[](const int index) const {
    return values[index];
  }

  __host__ __device__ inline float_t &operator[](const int index) {
    return values[index];
  }
};

float QuarticSolverCuda::solve_newtwon(const float *device_photons,
                                       float *device_sph_refl_points,
                                       const int total_number_of_photons,
                                       const int repetitions) {
  const auto grid_dim = dim3((total_number_of_photons + 255) / 256, repetitions);

  cudaEventRecord(start);
  quartic_solve_newtwon<<<grid_dim, 256>>>(
      device_photons, device_sph_refl_points, total_number_of_photons);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);

  float milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);

  return milliseconds;
}

/** Horner's method to evaluate the polynomial and its derivatives with as
 * little math operations as possible. We use a template here to allow the
 * compiler to unroll the for loops and produce code that is free from branches
 * and optimized for the grade of polynomial and derivatives as necessary.
 */
__device__ inline float_t evalPolyHorner(const Array5 &a,
                                         const float_t x) noexcept {
  float res[2] = {a[0], 0.f};
  for (int j = 1; j <= ORDER; ++j) {
    res[0] = (res[0] * x) + a[j];
    const auto l = ORDER - j < DIFFGRADE ? ORDER - j : DIFFGRADE;
    for (int i = 1; i <= l; ++i) {
      res[i] = (res[i] * x) + res[i - 1];
    }
  }

  float_t l = 1.0f;
  for (int i = 2; i <= DIFFGRADE; ++i) {
    l *= float_t(i);
    res[i] *= l;
  }

  return res[0] / res[1];
}

/// (for order 4) a[0]x^4 + a[1]x^3 + a[2]x^2 + a[3]x + a[4]
__device__ inline float_t f4(const Array5 &a, const float_t x) noexcept {
  float_t res = a[0];
  for (int i = 1; i <= ORDER; ++i) {
    res = (res * x) + a[i];
  }
  return res;
}

/** Newton-Rhapson method for calculating the root of the rich polynomial.
 *  It uses the bisection method in the beginning to get close enough to the
 * root to allow the second stage newton method to converge faster. After 4
 * iterations of newton precision is as good as single precision floating point
 * will get you. We have introduced a few tuning parameters like the newton gain
 * factor and a slightly skewed bisection division, which in this particular
 * case help to speed things up.
 *  TODO: These tuning parameters have been found by low effort experimentation
 * on random input data. A more detailed study should be done with real data to
 * find the best values.
 */
__device__ inline float_t solve_quartic_newton_RICH(const Array5 &a) noexcept {

  // We start a bit off center since the distribution of roots tends to be more
  // to the left side
  float_t m = 0.2f;

  // Do the bisest loops.
  // Use N steps of bisection method to find starting point for newton.
  float_t l(0.0f), u(0.5f);
  for (int i = 0; i < BISECTITS; ++i) {
    // get sign comparison mask. true if opposite sign.
    const auto oSign = (signbit(f4(a, m)) ^ signbit(f4(a, l)));
    if (oSign) {
      u = m;
    } else {
      l = m;
    }
    // 0.4 instead of 0.5 to speed up convergence.
    // Most roots seem to be closer to 0 than to the extreme end
    m = (u + l) * 0.4f;
  }

  // Most of the times we are approaching the root of the polynomial from one
  // side and fall short by a certain fraction. This fraction seems to be
  // around 1.04 of the quotient which is subtracted from x. By scaling it up,
  // we take bigger steps towards the root and thus converge faster.
  // TODO: study this factor more closely it's pure guesswork right now. We
  // might get away with 3 iterations if we can find an exact value.
  for (int i = 0; i < NEWTONITS; ++i) {
    const auto res = evalPolyHorner(a, m);
    m -= gain * res;
  }

  // return the result
  return m;
}

__global__ void quartic_solve_newtwon(const float *device_photons,
                                      float *device_sph_refl_points,
                                      const int total_number_of_photons) {
  const RichPhotons photons{device_photons, total_number_of_photons};
  for (int i = blockIdx.x * blockDim.x + threadIdx.x;
       i < total_number_of_photons; i += blockDim.x * gridDim.x) {
    const auto emissionPoint = photons.emission_point(i);
    const auto CoC = photons.center_of_curve(i);

    // vector from mirror centre of curvature to assumed emission point
    const auto evec = emissionPoint - CoC;
    const float_t e2 = dot(evec, evec);

    // vector from mirror centre of curvature to virtual detection point
    const auto virtDetPoint = photons.virtual_det_point(i);
    const auto dvec = virtDetPoint - CoC;
    const float_t d2 = dot(dvec, dvec);

    // |e|^2 * |d|^2
    const auto ed2 = e2 * d2;

    // e.d dot product
    const auto eDotd = dot(evec, dvec);

    // // Handle case e and/or d have zero size.
    // // This actually cannot happen in reality as requires input detection
    // // point and/or emission point to be exactly the same as the CoC,
    // // which never happens. So skip.
    // if constexpr ( all_arithmetic_v<float_t> ) {
    //   // scalar
    //   if ( UNLIKELY( ed2 <= 0.0f ) ) { ed2 = eDotd = float_t( 1.0f ); }
    // } else if constexpr ( all_SIMD_v<float_t> ) {
    //   // SIMD
    //   const auto ed2mask = ( ed2 <= float_t::Zero() );
    //   if ( UNLIKELY( any_of( ed2mask ) ) ) {
    //     eDotd( ed2mask ) = float_t::One();
    //     ed2( ed2mask )   = float_t::One();
    //   }
    // } else {
    //   // If get here cause compilation failure.
    //   float_t::WillFail();
    // }

    const float_t cosgamma2 = (eDotd * eDotd) / ed2;
    const float_t singamma2 = 1.f - cosgamma2;
    const float_t dy2 = d2 * singamma2;
    const float_t dy = sqrtf(dy2);
    const float_t dx = sqrtf(d2 * cosgamma2);
    const float_t e = sqrtf(e2);
    const float_t edx = e + dx;
    const auto radius = photons.radius(i);
    const float_t r2 = radius * radius;

    // Fill array for quartic equation
    // Newton solver doesn't care about a0 being not 1.0. Remove costly division
    // and several multiplies. This has some downsides though. The a-values are
    // hovering around a numerical value of 10^15. single precision float max is
    // 10^37. A single square and some multiplies will push it over the limit of
    // what single precision float can handle. It's ok for the newton method,
    // but Halley or higher order Housholder will fail without this
    // normalization. const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 :
    // std::numeric_limits<float_t>::max() );
    const float_t dyrad2 = 2.f * dy * radius;
    const Array5 aa = {4.f * ed2, -(2.f * dyrad2 * e2),
                       ((dy2 * r2) + (edx * edx * r2) - (4.f * ed2)),
                       (dyrad2 * e * (e - dx)), ((e2 - r2) * dy2)};

    // Use optimized newton solver on quartic equation.
    const float_t sinbeta = solve_quartic_newton_RICH(aa);

    // construct rotation transformation
    // Set vector magnitude to radius
    // rotate vector and update reflection point
    // rotation matrix uses sin(beta) and cos(beta) to perform rotation
    // even fast_asinf (which is only single precision and defeats the purpose
    // of this class being templatable to double btw) is still too slow
    // plus there is a cos and sin call inside AngleAxis ...
    // We can do much better by just using the cos(beta) we already have to
    // calculate sin(beta) and do our own rotation. On top of that we rotate
    // non-normalized and save several Divisions by normalizing only once at the
    // very end. Again, care has to be taken since we are close to float_max
    // here without immediate normalization. As far as we have tried with
    // extreme values in the rich coordinate systems this is fine.
    const float_t nx = (evec.y * dvec.z) - (evec.z * dvec.y);
    const float_t ny = (evec.z * dvec.x) - (evec.x * dvec.z);
    const float_t nz = (evec.x * dvec.y) - (evec.y * dvec.x);
    const float_t nx2 = nx * nx;
    const float_t ny2 = ny * ny;
    const float_t nz2 = nz * nz;
    const float_t n2 = nx2 + ny2 + nz2;

    const float_t enorm = radius / (e * n2);

    const float_t a = sinbeta * sqrtf(n2);
    const float_t b = (1.f - sqrtf(1.f - (sinbeta * sinbeta)));

    const float_t bnxny = b * nx * ny;
    const float_t bnxnz = b * nx * nz;
    const float_t bnynz = b * ny * nz;

    // non-normalized rotation matrix
    const float_t M[9] = {
        n2 - b * (nz2 + ny2), (a * nz) + bnxny,     (-a * ny) + bnxnz,     //
        (-a * nz) + bnxny,    n2 - b * (nx2 + nz2), (a * nx) + bnynz,      //
        (a * ny) + bnxnz,     (-a * nx) + bnynz,    n2 - (b * (ny2 + nx2)) //
    };

    // re-normalize rotation and scale to radius in one step
    const float_t ex = enorm * (evec.x * M[0] + evec.y * M[3] + evec.z * M[6]);
    const float_t ey = enorm * (evec.x * M[1] + evec.y * M[4] + evec.z * M[7]);
    const float_t ez = enorm * (evec.x * M[2] + evec.y * M[5] + evec.z * M[8]);

    // set the final reflection point
    device_sph_refl_points[i] = CoC.x + ex;
    device_sph_refl_points[total_number_of_photons + i] = CoC.y + ey;
    device_sph_refl_points[2 * total_number_of_photons + i] = CoC.z + ez;
  }
}

__device__ inline float_t solve_quartic_RICH(const float_t a, const float_t b,
                                             const float_t c, const float_t d) {

  constexpr const float_t r4 = 1.0 / 4.0;
  constexpr const float_t q2 = 1.0 / 2.0;
  constexpr const float_t q8 = 1.0 / 8.0;
  constexpr const float_t q1 = 3.0 / 8.0;
  constexpr const float_t q3 = 3.0 / 16.0;
  // const float_t UU { -( std::sqrt((float_t)3.0) / (float_t)2.0 ) };
  constexpr const float_t UU = -0.866025404; // - sqrt(3)/2

  const auto aa = a * a;
  const auto pp = b - q1 * aa;
  const auto aar4 = aa * r4;
  const auto qq = c - q2 * a * (b - aar4);
  const auto rr = d - r4 * (a * c - aar4 * (b - q3 * aa));
  const auto rc = q2 * pp;
  const auto rc2 = rc * rc;
  const auto sc = r4 * (r4 * pp * pp - rr);
  const auto tc = -powf(q8 * qq, 2.f);

  const auto qcub = (rc2 - 3 * sc);
  const auto rcub = (rc * ((2 * rc2) - (9 * sc)) + (27 * tc));

  const auto Q = qcub / 9.0;
  const auto R = rcub / 54.0;

  const auto Q3 = Q * Q * Q;
  const auto R2 = R * R;

  // const auto sgnR = ( R >= 0 ? -1 : 1 );
  // const auto A = sgnR * my_cbrt( (float_t)( fabs(R) + std::sqrt(fabs(R2-Q3))
  // ) ); Sign check above not needed for the specific solutions seen here
  const auto A = cbrtf((float_t)(fabsf(R) + sqrtf(fabsf(R2 - Q3))));

  const auto B = Q / A;

  const auto u1 = -0.5f * (A + B) - rc / 3.0f;
  const auto u2 = UU * fabsf(A - B);

  // Implementation with CUDA
  const auto w1 = thrust::sqrt(thrust::complex<float_t>(u1, u2));
  const auto w2 = thrust::sqrt(thrust::complex<float_t>(u1, -u2));
  const auto V = w1 * w2;
  const thrust::complex<float_t> w3 =
      (fabsf(V.real()) != 0.0f ? (float_t)(qq * -0.125f) / V
                               : thrust::complex<float_t>(0.f, 0.f));
  const float_t res = w1.real() + w2.real() + w3.real() - r4 * a;

  // thrust::complex<float> c = thrust::complex<float>(2.0f, 5.0f);
  // thrust::complex<float> c2 = c*c;
  // float r = c2.real();

  // // Implementation using STL classes
  // const auto w1 = std::sqrt( std::complex<float_t>(u1, u2) );
  // const auto w2 = std::sqrt( std::complex<float_t>(u1,-u2) );
  // const auto  V = w1 * w2;
  // const std::complex<float_t> w3 = ( std::abs(V) != 0.0 ? (float_t)( qq *
  // -0.125 ) / V :
  //                                 std::complex<float_t>(0,0) );
  // const float_t res = std::real(w1) + std::real(w2) + std::real(w3) - (r4*a);

  // // (Much) faster vectorised implementation using VectorClass
  // using Complex4x   = float_tname std::conditional<std::is_same<float_t,
  // float>::value, Complex4f, Complex4d>::float_t; using Complex2x   =
  // float_tname std::conditional<std::is_same<float_t, float>::value,
  // Complex2f, Complex2d>::float_t; const Complex4x W = sqrtf( Complex4x( u1,
  // u2, u1, -u2 ) ); const auto      V = W.get_low() * W.get_high(); const auto
  // w3 =
  //     ( fabs( V.extract( 0 ) ) > 0 || fabs( V.extract( 1 ) ) > 0 ? ( qq *
  //     -0.125f ) / V : Complex2x( 0, 0 ) );
  // const float_t res = W.extract( 0 ) + W.extract( 2 ) + w3.extract( 0 ) - (
  // r4 * a );

  // return the final result
  return (res > 1.f ? 1.f : (res < -1.f ? -1.f : res));
}

float QuarticSolverCuda::solve(const float *device_photons,
                               float *device_sph_refl_points,
                               const int total_number_of_photons,
                               const int repetitions) {
  const auto grid_dim = dim3((total_number_of_photons + 255) / 256, repetitions);

  cudaEventRecord(start);
  quartic_solve<<<grid_dim, 256>>>(device_photons, device_sph_refl_points,
                                   total_number_of_photons);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);

  float milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);

  return milliseconds;
}

__global__ void quartic_solve(const float *device_photons,
                              float *device_sph_refl_points,
                              const int total_number_of_photons) {
  const RichPhotons photons{device_photons, total_number_of_photons};
  for (int i = blockIdx.x * blockDim.x + threadIdx.x;
       i < total_number_of_photons; i += blockDim.x * gridDim.x) {
    const auto emissionPoint = photons.emission_point(i);
    const auto CoC = photons.center_of_curve(i);

    // vector from mirror centre of curvature to assumed emission point
    const Vector evec(emissionPoint - CoC);
    const float_t e2 = dot(evec, evec);

    // vector from mirror centre of curvature to virtual detection point
    const auto virtDetPoint = photons.virtual_det_point(i);
    const Vector dvec(virtDetPoint - CoC);
    const float_t d2 = dot(dvec, dvec);

    // various quantities needed to create quartic equation
    // see LHCB/98-040 section 3, equation 3
    const auto ed2 = e2 * d2;
    const float_t cosgamma2 =
        (ed2 > 0.f ? powf(dot(evec, dvec), 2.f) / ed2 : 1.0f);

    const auto e = sqrtf(e2);
    const auto d = sqrtf(d2);
    const auto singamma = cosgamma2 < 1.0f ? sqrtf(1.0f - cosgamma2) : 0.0f;
    const auto cosgamma = sqrtf(cosgamma2);

    const auto dx = d * cosgamma;
    const auto dy = d * singamma;
    const auto radius = photons.radius(i);
    const auto r2 = radius * radius;
    const auto dy2 = dy * dy;
    const auto edx = e + dx;

    // Fill array for quartic equation
    const auto a0 = 4.0f * ed2;
    const auto inv_a0 =
        (a0 > 0.f ? 1.0f / a0 : std::numeric_limits<float_t>::max());
    const auto dyrad2 = 2.0f * dy * radius;
    const auto a1 = -(2.0f * dyrad2 * e2) * inv_a0;
    const auto a2 = ((dy2 * r2) + (edx * edx * r2) - a0) * inv_a0;
    const auto a3 = (dyrad2 * e * (e - dx)) * inv_a0;
    const auto a4 = ((e2 - r2) * dy2) * inv_a0;

    // use simplified RICH version of quartic solver
    const auto sinbeta = solve_quartic_RICH(a1, a2, a3, a4);

    // (normalised) normal vector to reflection plane
    const auto n = cross(evec, dvec);
    const auto nn = n * n;
    const auto n2 = n.x + n.y + n.z;

    const float_t enorm = radius / (e * n2);

    const float_t a = sinbeta * sqrtf(n2);
    const float_t b = (1.f - sqrtf(1.f - (sinbeta * sinbeta)));

    const float_t bnxny = b * n.x * n.y;
    const float_t bnxnz = b * n.x * n.z;
    const float_t bnynz = b * n.y * n.z;

    // non-normalized rotation matrix
    const float_t M[9] = {
        n2 - b * (nn.z + nn.y),  (a * n.z) + bnxny,
        (-a * n.y) + bnxnz, //
        (-a * n.z) + bnxny,      n2 - b * (nn.x + nn.z),
        (a * n.x) + bnynz, //
        (a * n.y) + bnxnz,       (-a * n.x) + bnynz,
        n2 - (b * (nn.y + nn.x)) //
    };

    // re-normalize rotation and scale to radius in one step
    const float_t ex = enorm * (evec.x * M[0] + evec.y * M[3] + evec.z * M[6]);
    const float_t ey = enorm * (evec.x * M[1] + evec.y * M[4] + evec.z * M[7]);
    const float_t ez = enorm * (evec.x * M[2] + evec.y * M[5] + evec.z * M[8]);

    // set the final reflection point
    device_sph_refl_points[i] = CoC.x + ex;
    device_sph_refl_points[total_number_of_photons + i] = CoC.y + ey;
    device_sph_refl_points[2 * total_number_of_photons + i] = CoC.z + ez;
  }
}
