#include "Tools.cuh"

struct QuarticSolverCuda {
  cudaEvent_t start, stop;

  QuarticSolverCuda() {
    cudaCheck(cudaEventCreate(&start));
    cudaCheck(cudaEventCreate(&stop));
  }

  float solve(const float *device_photons, float *device_sph_refl_points,
              const int total_number_of_photons, const int repetitions);

  float solve_newtwon(const float *device_photons,
                      float *device_sph_refl_points,
                      const int total_number_of_photons,
                      const int repetitions);
};

__global__ void quartic_solve(const float *device_photons,
                              float *device_sph_refl_points,
                              const int total_number_of_photons);

__global__ void quartic_solve_newtwon(const float *device_photons,
                                      float *device_sph_refl_points,
                                      const int total_number_of_photons);
