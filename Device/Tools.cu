#include "QuarticSolver.cuh"
#include <iomanip>
#include <cstdio>

int set_device(const int cuda_device)
{
  int n_devices = 0;
  cudaDeviceProp device_properties;
  cudaCheck(cudaGetDeviceCount(&n_devices));
  std::cout << "There are " << n_devices << " devices available\n";
  for (int cd = 0; cd < n_devices; ++cd) {
    cudaDeviceProp device_properties;
    cudaCheck(cudaGetDeviceProperties(&device_properties, cd));
    std::cout << std::setw(3) << cd << " " << device_properties.name << "\n";
  }
  if (cuda_device >= n_devices) {
    std::cerr << "Chosen device (" << cuda_device << ") is not available.\n";
    return -1;
  }
  std::cout << "\n";
  cudaCheck(cudaSetDevice(cuda_device));
  cudaCheck(cudaGetDeviceProperties(&device_properties, cuda_device));
  if (n_devices == 0) {
    std::cerr << "Failed to select device " << cuda_device << "\n";
    return -1;
  }
  else {
    std::cout << "Selected device " << cuda_device << ": " << device_properties.name
               << "\n\n";
  }
  return 0;
}
