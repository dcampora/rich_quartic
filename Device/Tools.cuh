#include <cuda_runtime.h>
#include <iostream>

#define cudaCheck(stmt)                                \
  {                                                    \
    cudaError_t err = stmt;                            \
    if (err != cudaSuccess) {                          \
      std::cerr << "Failed to run " << #stmt << "\n";  \
      throw std::invalid_argument("cudaCheck failed"); \
    }                                                  \
  }

int set_device(const int cuda_device);
