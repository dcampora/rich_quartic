/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// SIMD types
#include "RichSIMDTypes.h"

// Gaudi
//#include "GaudiKernel/Transform3DTypes.h"
#include "Point3DTypes.h"
#include "Vector3DTypes.h"

// Quartic Solver
#include "QuarticSolverNewton.h"

// STL
#include <iostream>
#include <random>
#include <string>
#include <typeinfo>
#include <vector>

// CUDA QuarticSolver
#include "QuarticSolver.cuh"

#include "Common.h"
#include "tbb/tbb.h"

// Make an instance of the quartic solver
Rich::Rec::QuarticSolverNewton qSolver;

using DoubleV = Rich::SIMD::FP<double>;
using FloatV  = Rich::SIMD::FP<float>;

ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<float>>   sphReflPointF;
ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<double>>  sphReflPointD;
ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<FloatV>>  sphReflPointFSIMD;
ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<DoubleV>> sphReflPointDSIMD;

template <typename TYPE, typename GENTYPE = TYPE>
class Data {
public:
  using Vector = Rich::SIMD::STDVector<Data>;
  using Scalar = TYPE;

public:
  ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<TYPE>> emissPnt;
  ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<TYPE>> centOfCurv;
  ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<TYPE>> virtDetPoint;
  TYPE                                                        radius;

public:
  Data() {
    // random generator
    static std::default_random_engine gen;
    // Distributions for each member
    static std::uniform_real_distribution<GENTYPE> r_emiss_x( -800, 800 ), r_emiss_y( -600, 600 ),
        r_emiss_z( 10000, 10500 );
    static std::uniform_real_distribution<GENTYPE> r_coc_x( -3000, 3000 ), r_coc_y( -20, 20 ), r_coc_z( 3300, 3400 );
    static std::uniform_real_distribution<GENTYPE> r_vdp_x( -3000, 3000 ), r_vdp_y( -200, 200 ), r_vdp_z( 8100, 8200 );
    static std::uniform_real_distribution<GENTYPE> r_rad( 8500, 8600 );
    // setup data
    emissPnt     = {r_emiss_x( gen ), r_emiss_y( gen ), r_emiss_z( gen )};
    centOfCurv   = {r_coc_x( gen ), r_coc_y( gen ), r_coc_z( gen )};
    virtDetPoint = {r_vdp_x( gen ), r_vdp_y( gen ), r_vdp_z( gen )};
    radius       = r_rad( gen );
  }
};

template <typename DATA, typename POINT>
inline void __attribute__( ( always_inline ) ) //
solve( const DATA& data, POINT& sphReflPoint ) {
  // printf("{%f, %f, %f}, {%f, %f, %f}, {%f, %f, %f}, %f: ",
  //   data.emissPnt.x(), data.emissPnt.y(), data.emissPnt.z(),
  //   data.centOfCurv.x(), data.centOfCurv.y(), data.centOfCurv.z(),
  //   data.virtDetPoint.x(), data.virtDetPoint.y(), data.virtDetPoint.z(),
  //   data.radius);

  qSolver.solve<typename DATA::Scalar, 3, 2>( data.emissPnt, data.centOfCurv, data.virtDetPoint, data.radius,
                                              sphReflPoint );

  // printf("{%f, %f, %f}\n", sphReflPoint.x(), sphReflPoint.y(), sphReflPoint.z());
}

template <std::size_t NTESTS, typename DATA, typename POINT>
void __attribute__( ( noinline ) )
solveV( const typename DATA::Vector& dataV, std::vector<POINT>& sphReflPoint ) {
  for ( std::size_t i = 0; i < NTESTS; ++i ) {
    // iterate over the data and solve it...
    for (std::size_t j = 0; j < dataV.size(); ++j) {
      solve( dataV[j], sphReflPoint[j] );
    }
  }
}

int main( int /*argc*/, char** /*argv*/ ) {
  // Set the accelerator device
  set_device(0);

  constexpr std::size_t number_of_photons  = 48e3;
  constexpr std::size_t host_repetitions   = 5000;
  constexpr std::size_t device_repetitions = 50000;
  constexpr std::size_t number_of_tasks    = 20;
  constexpr std::size_t total_number_of_photons = number_of_photons * number_of_tasks;

  static_assert( ( number_of_photons % FloatV::Size ) == 0 && ( number_of_photons % DoubleV::Size ) == 0 );

  std::cout << "Creating " << total_number_of_photons << " random photons...";

  // Scalar photons
  std::vector<Data<float>::Vector> dataVF (number_of_tasks);
  std::vector<std::vector<ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<float>>>> resultVF (number_of_tasks);
  for (std::size_t i = 0; i < number_of_tasks; ++i) {
    resultVF[i].resize(number_of_photons);
    dataVF[i].reserve(number_of_photons);
    for (std::size_t j = 0; j < number_of_photons; ++j) {
      dataVF[i].emplace_back();
    }
  }

  // SIMD photons
  using FVSIMD = Data<FloatV, float>;
  std::vector<FVSIMD::Vector> dataVFSIMD (number_of_tasks);
  std::vector<std::vector<ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<FloatV>>>> resultVFSIMD (number_of_tasks);
  // note we scale the SIMD vector sizes so that they have the same
  // equivalent scalar entries
  const auto nPhotsFSIMD = number_of_photons / FloatV::Size;
  for (std::size_t i = 0; i < number_of_tasks; ++i) {
    resultVFSIMD[i].resize(nPhotsFSIMD);
    dataVFSIMD[i].reserve(nPhotsFSIMD);
    for (std::size_t j = 0; j < nPhotsFSIMD; ++j) {
      dataVFSIMD[i].emplace_back();
    }
  }

  // Device photons
  float* device_photons;
  float* device_sph_refl_points;
  cudaCheck(cudaMalloc((void**) &device_photons, total_number_of_photons * 10 * sizeof(float)));
  cudaCheck(cudaMalloc((void**) &device_sph_refl_points, total_number_of_photons * 3 * sizeof(float)));
  cudaCheck(cudaMemset(device_sph_refl_points, 0, total_number_of_photons * 3 * sizeof(float)));
  // Populate data as a plain SOA
  for (int i = 0; i < 10; ++i) {
    std::vector<float> v(total_number_of_photons);
    for (uint k = 0; k < number_of_tasks; ++k) {
      for (uint j = 0; j < number_of_photons; ++j) {
        v[k * number_of_photons + j] = reinterpret_cast<const float*>(dataVF[k].data())[i + j * 10];
      }
    }
    cudaCheck(cudaMemcpy(device_photons + i * total_number_of_photons, v.data(), v.size() * sizeof(float), cudaMemcpyHostToDevice));
  }

  std::cout << " done.\nInitializing threads...";

  tbb::parallel_for( std::size_t(0), number_of_tasks, []( const size_t) {} );

  std::cout << " done.\nRunning tests...\n";

  {
    // run the solver for scalar floats
    timespec start, end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    tbb::parallel_for( std::size_t(0), number_of_tasks, [&]( const size_t i ) {
      solveV<host_repetitions, Data<float>>(dataVF[i], resultVF[i]);
    });
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    const auto fTimeScalar = static_cast<double>(time_diff(&start, &end)) / 1000000000.0;
    const auto fThroughputScalar = (host_repetitions * total_number_of_photons) / fTimeScalar;
    std::cout << "Scalar test: " << fThroughputScalar << " photons/sec (" << (static_cast<float>(host_repetitions * total_number_of_photons)) << " photons in " << fTimeScalar << " seconds).\n";

    // run the solver for SIMD floats
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    tbb::parallel_for( std::size_t(0), number_of_tasks, [&]( const size_t i ) {
      solveV<host_repetitions, FVSIMD>(dataVFSIMD[i], resultVFSIMD[i]);
    });
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    const auto fTimeSIMD = static_cast<double>(time_diff(&start, &end)) / 1000000000.0;
    const auto fThroughputSIMD = (host_repetitions * total_number_of_photons) / fTimeSIMD;
    const auto speedupSIMD = fThroughputSIMD / fThroughputScalar;
    std::cout << "SIMD   test: " << fThroughputSIMD << " photons/sec, " << speedupSIMD << "x against scalar (" << (static_cast<float>(host_repetitions * total_number_of_photons)) << " photons in " << fTimeSIMD << " seconds).\n";

    // run the solver for device floats
    QuarticSolverCuda cuda_solver;
    const auto fTimeDevice = static_cast<double>(cuda_solver.solve_newtwon(device_photons, device_sph_refl_points, total_number_of_photons, device_repetitions)) / 1000.0;
    const auto fThroughputDevice = (device_repetitions * total_number_of_photons) / fTimeDevice;
    const auto speedupDevice = fThroughputDevice / fThroughputScalar;
    std::cout << "Device test: " << fThroughputDevice << " photons/sec, " << speedupDevice << "x against scalar (" << (static_cast<float>(device_repetitions * total_number_of_photons)) << " photons in " << fTimeDevice << " seconds).\n";

    // // Test the device actually did something:
    // std::vector<float> host_photons (10 * total_number_of_photons);
    // std::vector<float> host_sph_refl_points (3 * total_number_of_photons);
    // cudaCheck(cudaMemcpy(host_photons.data(), device_photons, total_number_of_photons * 10 * sizeof(float), cudaMemcpyDeviceToHost));
    // cudaCheck(cudaMemcpy(host_sph_refl_points.data(), device_sph_refl_points, total_number_of_photons * 3 * sizeof(float), cudaMemcpyDeviceToHost));
    // for (int i = 0; i < host_sph_refl_points.size() / 3; ++i) {
    //   printf("{%f, %f, %f}, {%f, %f, %f}, {%f, %f, %f}, %f: {%f, %f, %f}\n",
    //     host_photons[i], host_photons[total_number_of_photons + i], host_photons[2 * total_number_of_photons + i],
    //     host_photons[3 * total_number_of_photons + i], host_photons[4 * total_number_of_photons + i], host_photons[5 * total_number_of_photons + i],
    //     host_photons[6 * total_number_of_photons + i], host_photons[7 * total_number_of_photons + i], host_photons[8 * total_number_of_photons + i],
    //     host_photons[9 * total_number_of_photons + i],
    //     host_sph_refl_points[i], host_sph_refl_points[total_number_of_photons + i], host_sph_refl_points[2 * total_number_of_photons + i]);
    // }
  }

  cudaCheck(cudaDeviceReset());

  return 0;
}
