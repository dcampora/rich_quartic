message(STATUS "downloading...
     src='http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/vectorclass-1.30.tar.gz'
     dst='/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30.tar.gz'
     timeout='none'")




if("x" STREQUAL "x")
  file(DOWNLOAD
    "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/vectorclass-1.30.tar.gz"
    "/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30.tar.gz"
    # no EXPECTED_HASH
    # no TIMEOUT
    STATUS status
    LOG log)
else()
  get_filename_component(fname "/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30.tar.gz" NAME)
  if(EXISTS "/${fname}")
    get_filename_component(ddir "/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30.tar.gz" PATH)
    file(COPY "/${fname}" DESTINATION "${ddir}")
    set(status 0 "copy from cache")
  else()
    file(DOWNLOAD
      "http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/vectorclass-1.30.tar.gz"
      "/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30.tar.gz"
      # no EXPECTED_HASH
      # no TIMEOUT
      STATUS status
      LOG log)
    file(COPY "/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30.tar.gz" DESTINATION "")
  endif()
endif()

list(GET status 0 status_code)
list(GET status 1 status_string)

if(NOT status_code EQUAL 0)
  message(FATAL_ERROR "error: downloading 'http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/sources/vectorclass-1.30.tar.gz' failed
  status_code: ${status_code}
  status_string: ${status_string}
  log: ${log}
")
endif()

message(STATUS "downloading... done")
