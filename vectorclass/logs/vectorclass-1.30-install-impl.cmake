set(ENV{VS_UNICODE_OUTPUT} "")
set(command "/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.11.1/Linux-x86_64/bin/cmake;-E;copy_directory;/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass/1.30;/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/install/vectorclass/1.30/x86_64-centos7-gcc8-opt")


execute_process(COMMAND ${command} RESULT_VARIABLE result)
if(result)
  set(msg "Command failed (${result}):\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach()
  message(FATAL_ERROR "${msg}")
endif()
set(command "/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/lcgcmake/externals/vectorclass_postinstall.sh;/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/install/vectorclass/1.30/x86_64-centos7-gcc8-opt")


execute_process(COMMAND ${command} RESULT_VARIABLE result)
if(result)
  set(msg "Command failed (${result}):\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach()
  message(FATAL_ERROR "${msg}")
endif()
