


set(ENV{VS_UNICODE_OUTPUT} "")
set(command "/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.11.1/Linux-x86_64/bin/cmake;-Dmake=${make};-Dconfig=${config};-P;/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30-stamp/vectorclass-1.30-install-impl.cmake")
execute_process(
  COMMAND ${command}
  RESULT_VARIABLE result
  OUTPUT_VARIABLE logs
  ERROR_VARIABLE logs
  )
file(WRITE "/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30-stamp/vectorclass-1.30-install.log" ${logs} )
if(result)
  set(msg "Command failed: ${result}\n")
  foreach(arg IN LISTS command)
    set(msg "${msg} '${arg}'")
  endforeach()
  file(SHA1 "/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30-stamp/vectorclass-1.30-install.log" sha1)
  set(msg "${msg}\nSee also\n  /mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30-stamp/vectorclass-1.30-install.log [${sha1}]\n")
  file(APPEND /mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/fail-logs.txt "/mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30-stamp/vectorclass-1.30-install.log\n")
  message(FATAL_ERROR "${msg}")
else()
  set(msg "vectorclass-1.30 install command succeeded.  See also /mnt/build/jenkins/workspace/lcg_hsf_build/BUILDTYPE/Release/COMPILER/gcc8binutils/LABEL/centos7/build/externals/vectorclass-1.30/src/vectorclass-1.30-stamp/vectorclass-1.30-install.log\n")
  message(STATUS "${msg}")
endif()
